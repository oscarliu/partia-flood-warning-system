from floodsystem.geo import stations_within_radius
from floodsystem.stationdata import build_station_list

def run():
    stations = build_station_list()
    coordinate = (52.2053, 0.1218)
    result = stations_within_radius(stations,coordinate,10)
    for i in result:
        print(i)
if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")

    # Run Task1C
    run()